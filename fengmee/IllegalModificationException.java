package fengmee;

import be.kuleuven.cs.som.annotate.Basic;

public class IllegalModificationException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Initialize this new illegal modification exception with given index and status.
	 * 
	 * @param  	index
	 *         	The time slot index for this new illegal modification exception.
	 * @param	status
	 * 			The status of the time slot for the designated index.
	 * @post   	The index of this new illegal modification exception is equal
	 *         	to the given index.
	 *       	| new.getIndex() == index
	 * @post	The occupation status of this new illegal modification exception is equal to
	 * 			the give occupation status .
	 * 			| new.getStatus() == status
	 */
	public IllegalModificationException(int index, boolean status) {
		this.index = index;
		this.status = status;
	}

	/**
	 * Return the index registered for this illegal modification exception.
	 */
	@Basic
	public int getIndex() {
		return this.index;
	}

	/**
	 * Variable registering the index involved in this illegal modification exception.
	 */
	protected final int index;
	
	/**
	 * 
	 * @return
	 */
	@Basic
	public boolean getStatus(){
		return this.status;
	}
	
	/**
	 * 
	 */
	protected final boolean status;

}
