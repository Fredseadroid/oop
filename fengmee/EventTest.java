package fengmee;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class EventTest {
	private Agenda agenda1;
	private Event eventDay11Holiday;
	private Event eventDay11Birthday;

	@Before
	public void setUp() throws Exception {
		agenda1 = new Agenda("agenda1");
		eventDay11Holiday = new Event (agenda1,"eventDay11Holiday",11);
		eventDay11Birthday = new Event ("eventDay11Birthday",11);
	}
	
	@Test
	public void testEventAgendaStringLong() {
		Event newEvent1 = new Event(agenda1,"newEvent1",9);
		assertTrue(newEvent1.getDescription().equals("newEvent1"));
		assertEquals(newEvent1.getDay(),9);
		assertTrue(newEvent1.hasAsAgenda(agenda1));
		assertTrue(newEvent1.getAllAgenda().contains(agenda1));
	}

	@Test
	public void testEventStringLong() {
		Event newEvent2 = new Event("newEvent2",9);
		assertTrue(newEvent2.getDescription().equals("newEvent2"));
		assertEquals(newEvent2.getDay(),9);
		assertFalse(newEvent2.getAllAgenda().contains(agenda1));
	}

	@Test
	public void testMoveNbDays_forwardValidCase() {
		eventDay11Holiday.moveNbDays(3);
		assertEquals(eventDay11Holiday.getDay(),14);
		assertTrue(eventDay11Holiday.hasAsAgenda(agenda1));
		assertTrue(agenda1.getEntriesAt(14).contains(eventDay11Holiday));
		assertFalse(agenda1.getEntriesAt(11).contains(eventDay11Holiday));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testMoveNbDays_forwardInvalidCase() throws Exception{
		eventDay11Holiday.moveNbDays(3000);
		assertEquals(eventDay11Holiday.getDay(),11);
		assertTrue(eventDay11Holiday.hasAsAgenda(agenda1));
		assertFalse(agenda1.getEntriesAt(3011).contains(eventDay11Holiday));
		assertTrue(agenda1.getEntriesAt(11).contains(eventDay11Holiday));
	}
	
	@Test
	public void testMoveNbDays_backwardValidCase() {
		eventDay11Holiday.moveNbDays(-3);
		assertEquals(eventDay11Holiday.getDay(),8);
		assertTrue(eventDay11Holiday.hasAsAgenda(agenda1));
		assertTrue(agenda1.getEntriesAt(8).contains(eventDay11Holiday));
		assertFalse(agenda1.getEntriesAt(11).contains(eventDay11Holiday));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testMoveNbDays_backwardInvalidCase() throws Exception{
		eventDay11Holiday.moveNbDays(-15);
		assertEquals(eventDay11Holiday.getDay(),11);
		assertTrue(eventDay11Holiday.hasAsAgenda(agenda1));
		assertFalse(agenda1.getEntriesAt(-4).contains(eventDay11Holiday));
		assertTrue(agenda1.getEntriesAt(11).contains(eventDay11Holiday));
	}

	@Test
	public void testOverlapsWith() {
		assertFalse(eventDay11Birthday.overlapsWith(eventDay11Holiday));
	}

	@Test
	public void testIsOccupied() {
		assertFalse(eventDay11Birthday.isOccupied(12));
	}
}
