/**
 * 
 */
package fengmee;

import java.util.*;

import be.kuleuven.cs.som.annotate.*;

/**
 * An abstract class of slotted entries with long days, integer time slots, and string descriptions.
 * 
 * @invar	The day of each entry must be a valid day.
 * 			|canHaveAsDay(getDay())
 * @invar	The description of the entry must be a valid string.
 * 			|isValidDescription(getDescription())
 * @invar	The entry must be associated with proper agendas.
 * 			|hasProperAgendas()
 * 
 * @version	3.2
 * @author	Aimee Backiel and Feng Wu
 */
abstract class SlottedEntry extends Entry {
	/**
	 * Initialize this new entry with given description, day, and a series of time slot.
	 * 
	 * @param	agenda
	 * 			The agenda which the entry will be registered in.
	 * @param 	description
	 * 		  	The description of this new agenda entry.
	 * @param 	day
	 * 		  	The day of this new agenda entry.
	 * @param 	slots
	 * 		  	The series of time slot which this new entry occupied.
	 * @effect	The day will be registered using setDay(day)
	 * 			|setDay(day)
	 * @effect	The description will be registered using setDescription(description)
	 * 			|setDescription(description)
	 * @effect	The slots will be registered using addSlots(slots)
	 * 			|addSlots(slots)
	 * @effect	If the provided agenda is not null, register it using agenda.addEntry(this)
	 * 			|if (agenda != null)
	 * 			|	agenda.addEntry(this)
	 * @note	The day of the entry must be initialized before being added to any agenda.
	 */
	@Model
	protected SlottedEntry(Agenda agenda, String description, long day, int... slots) 
			throws IllegalSlotException, IllegalModificationException, IllegalArgumentException{
		setDescription(description);
		setDay(day);
		addSlots(slots);
		if (agenda != null){
			agenda.addEntry(this);
		}
	}
	
	/**
	 * Initialize this new entry with given description, day, and a series of time slot.
	 * 
	 * @param 	description
	 * 		  	The description of this new agenda entry.
	 * @param 	day
	 * 		  	The day of this new agenda entry.
	 * @param 	slots
	 * 		  	The series of time slot which this new entry occupied.
	 * @effect	Construct this entry using Entry(null, description, day, slots)
	 * 			|SlottedEntry(null, description, day, slots)
	 */
	@Model
	protected SlottedEntry (String description, long day, int... slots) 
			throws IllegalSlotException, IllegalModificationException {
		this(null, description, day, slots);
	}
	
	/**
	 * Initialize this new entry with given description and day, to occupy all time slots of that day.
	 * 
	 * @param 	description
	 * 		  	The description of this new agenda entry.
	 * @param 	day
	 * 		  	The day of this new agenda entry.
	 * @effect	Construct this entry using Entry(description, day, 1)
	 * 			|SlottedEntry(description, day, 1)
	 * @Post	All slots for that day will be occupied.
	 * 			|for (int i=1 ; i <= getSlotsPerDay() ; i++)
	 * 			|	new.isOccupied(i) == true;
	 */
	@Model
	protected SlottedEntry (String description, long day) 
			throws IllegalSlotException, IllegalModificationException {
		this(description,day,1);
		for (int i=2 ; i <= getSlotsPerDay() ; i++){
			this.addSlots(i);
		}
	}
	
	/**
	 * Return the number of time slots each day has for the agenda system.
	 */
	@Basic @Immutable
	public static int getSlotsPerDay(){
		return slotsPerDay;
	}//getter
	
	/**
	 * The variable stores how many slots are available in each day.
	 * @note	Later on if needed we can construct corresponding setter for it.
	 */
	private static final int slotsPerDay = 24;
	
	/**
	 * Check whether the time slot is valid for registering.
	 * 
	 * @param 	slot
	 * 			The time slot to be checked.
	 * @return	Slot must be between 1 and SlotsPerDay.
	 * 			|result == ((slot <= getSlotsPerDay()) && (slot > 0))
	 */
	public static boolean isValidSlot(int slot){
		return ((slot <= getSlotsPerDay()) && (slot > 0));
	}//checker
	
	/**
	 * Check whether the time slot is occupied already.
	 * 
	 * @param 	index
	 * 			The index of the slot to be checked.
	 * @return	True if the designated time slot is occupied, otherwise false.
	 * 			|result == 
	 * 			|	(for some slot in getAllSlots():
	 * 			|		index == slot )
	 * @throws	IllegalSlotException
	 * 			If the given time slot if out of the range of valid time slots.
	 * 			|!isValidSlot(index)
	 */ 
	@Override
	public boolean isOccupied (int index) throws IllegalSlotException{
		if (!isValidSlot(index))
			throw new IllegalSlotException(index);
		return this.slots[index-1];
	}//getter
	
	/**
	 * Return all occupied slots into an integer array with ascending order.
	 */
	@Basic 
	public int[] getAllSlots(){
		int[] allSlots = new int[getNbSlots()];
		int number = 0;
		for (int i = 1; i <= getSlotsPerDay(); i++){
			if (isOccupied(i) == true){
				allSlots[number]=i;
				number++;
			}
		}
		return allSlots;
	}

	/**
	 * get the total number of time slots that are occupied by the entry.
	 * 
	 * @return	The number of time slots that are occupied.
	 * 			|result == count if (for each i from 1 to getSlotsPerDay(), isOccupied(i) == true)
	 */
	public int getNbSlots(){
		int number = 0;
		for (int i = 1; i <= getSlotsPerDay(); i++){
			if (isOccupied(i) == true){
				number++;
			}
		}
		return number;
	}

	/**
	 * get the first time slot that is occupied by the entry.
	 * 
	 * @return	The time slot number which is the first occupied one for the current entry.
	 * 			|result == i
	 * 			|	for any index < i
	 * 			|		isOccupied(index) == false
	 */
	public int getFirstSlot(){
		return getAllSlots()[0];
	}

	/**
	 * get the last time slot that is occupied by the entry.
	 * 
	 * @return	The time slot number which is the last occupied one for the current entry.
	 * 			|result == i
	 * 			|	for any index > i
	 * 			|		isOccupied(index)==false
	 */
	public int getLastSlot(){
		int[] slotsArray = getAllSlots();
		return slotsArray[slotsArray.length-1];
	}

	/**
	 * Register a series of integers as occupied time slots of the entry.
	 * 
	 * @param 	slots
	 * 			A series of time slots which the entry will occupy.
	 * @effect	Occupy the designated array of time slots using method addSlot.
	 * 			|for each slot in slots:
	 * 			|	setSlot(slot,true)
	 * @throws	IllegalArgumentException
	 * 			...
	 * 			|!canAddAsSlots(slots)
	 */
	public void addSlots(int... slots) 
			throws IllegalSlotException, IllegalModificationException, IllegalArgumentException{
		if (!canAddAsSlots(slots)){
			throw new IllegalArgumentException();
		}
		for (int slot: slots){
			setSlot(slot, true);
		}
	}
	
	/**
	 * Check whether the given slots can be added to the slotted entry.
	 * 
	 * @param 	slots
	 * 			The array of slots to be checked
	 * @return	If the provided array is null, return false.
	 * 			|if (slots == null)
	 * 			|	result == false;
	 * 			If one or more of the provided slots are occupied, return false.
	 * 			|for some agenda in getAllAgenda() : 
	 * 			|	if (!agenda.slotsAvailable(getDay(),slots))
	 * 			|		result == false
	 */
	public boolean canAddAsSlots(int... slots){
		if (slots == null){
			return false;
		}
		//if overlap, return false
		for (Agenda agenda : getAllAgenda()){
			if (!agenda.slotsAvailable(getDay(),slots)){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Unregister the designated time slot for the entry.
	 * 
	 * @param 	slot
	 * 			A time slot which the entry will no longer occupy.
	 * @effect	The boolean value in the slots array at index slot will be false.
	 * 			|setSlot(slot,false)
	 * @throws	IllegalArgumentException
	 * 			Possibly if the slot to be removed is not the first slot or last slot
	 * 			| ? (slot != this.getFirstSlot()) && (slot != this.getLastSlot())
	 */
	public void removeSlot(int slot)
		throws IllegalSlotException, IllegalModificationException, IllegalArgumentException {
		setSlot(slot, false);
	}
		
	/**
	 * Set the designated time slot to certain status. 
	 * 
	 * @param 	slot
	 * 			Index for the time slot to be modified.
	 * @param 	occupationStatus
	 * 			Status that the designated time slot to be set.
	 * @post	Set the status of the time slot to the provided status.
	 * 			|new.slots[slot-1] == occupationStatus
	 * @throws 	IllegalSlotException
	 * 			When the provided time slot index is out of range, throw this exception.
	 * 			|!isValidSlot(slot)
	 * @throws	IllegalModificationException
	 * 			When the time slot is already in the status of the provided status, throw this exception.
	 * 			|isOccupied(slot) == occupationStatus
	 */
	@Model
	private void setSlot(int slot, boolean occupationStatus) 
			throws IllegalSlotException, IllegalModificationException {
		if (!isValidSlot(slot))
			throw new IllegalSlotException(slot);
		if(isOccupied(slot) == occupationStatus)
			throw new IllegalModificationException(slot, isOccupied(slot));
		this.slots[slot-1] = occupationStatus;
	}//setter

	/**
	 * variable that stores the status of each slot for each day.
	 */
	private final boolean[] slots = new boolean[getSlotsPerDay()];
	
	/**
	 * check whether this entry has at least one slot (on the same day) in common with some other entry.
	 * 
	 * @param 	other
	 * 			The other entry to be checked with.
	 * @return 	If the other entry is null, return false.
	 * 			|if(other == null)
	 * 			|	result == false
	 * @return	If this entry and the other entry are not in the same day, return false.
	 * 			|else if (this.getDay() != other.getDay())
	 * 			|	result == false
	 * @return	True if the same time slots from two entries are both occupied.
	 * 			|else result ==
	 * 			|		(for some i from 1 to getSlotsPerDay(): 
	 * 			|			this.isOccupied(i) && other.isOccupied(i)) 
	 */
	@Override
	public boolean overlapsWith(Entry other){
		if (other == null){
			return false;
		}
		if (this.getDay() == other.getDay()){
			for (int i = 1; i <= getSlotsPerDay(); i++){
				if (this.isOccupied(i) && other.isOccupied(i)){
					return true;
				}
			}//Slotted entry will always overlap with itself.
		}
		return false;
	}

	/**
	 * check whether this entry starts before some other entry.
	 * 
	 * @param 	other
	 * 			The other entry to be checked.
	 * @return	False if the other entry is null.
	 * 			|if (other == null)
	 * 			|	result == false
	 * @return	True if the day registered for the current entry is earlier than the other one.
	 * 			|if ((this.getDay() < other.getDay())
	 * 			|	result == true
	 * @return	Or in the same day but the first occupied time slot of the current entry is earlier,
	 * 			Otherwise false.
	 * 			|else result == (this.getDay() == other.getDay()) 
	 * 			|				&& (other instanceof SlottedEntry)
	 * 			|				&& (this.getFirstSlot() < other.getFirstSlot())
	 */
	@Override
	public boolean startsBefore(Entry other){
		if (other == null){
			return false;
		}
		if (super.startsBefore(other)){
			return true;
		}
		else {
			if ((this.getDay()== other.getDay()) && (other instanceof SlottedEntry)){
				if(this.getFirstSlot() < ((SlottedEntry)other).getFirstSlot()){
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Moves an entry the given number of slots, either ahead if positive or back if negative.
	 * 
	 * @param 	slotsToMove
	 * 			The number that all the time slots associated with this entry should be moved.
	 * @post	The time slots on which this entry registered moved according to slotsToMove.
	 * 			|for (i = 0, i < getNbSlots(), i++)
	 * 			|	new.getAllSlots()[i] == (this.getAllSlots()[i] + slotsToMove)
	 * @throws	IllegalArgumentException
	 * 			If the result will be out of range.
	 * 			|((getLastSlot()+slotsToMove) > getSlotsPerDay()) ||
	 * 			|((getFirstSlot()+slotsToMove) <= 0)
	 * 			Or if the move will result in overlapping.
	 * 			| for some agenda in getAllAgenda():
	 * 			| 	for (i = 0; i < GetAllSlots().length; i++)
	 * 			|		!agenda.slotAvailable(this.getDay(),GetAllSlots()[i] + slotsToMove)
	 */
	public void moveNbSlots (int slotsToMove) throws IllegalArgumentException{
		if ((getLastSlot()+slotsToMove) > getSlotsPerDay()){
			throw new IllegalArgumentException();
		}
		if ((getFirstSlot()+slotsToMove) <= 0){
			throw new IllegalArgumentException();
		}
		int[] movedSlots = getAllSlots();
		for (int i = 0; i < getAllSlots().length; i++){
			movedSlots[i] += slotsToMove;
		}
		//empty all the time slots of the entry.
		for (int i = 1; i <= getSlotsPerDay(); i++){
			if(isOccupied(i) == true){
				removeSlot(i);
			}
		}
		try {
			addSlots(movedSlots);
		}//register moved time slots to the entry.
		catch (IllegalArgumentException exc){
			for (int i = 0; i < movedSlots.length; i++){
				movedSlots[i] -= slotsToMove;
			}
			addSlots(movedSlots);
			throw exc;
		}//if not success, restore the original status.
	}

	/**
	 * Move the day number registered for this entry for designated days.
	 * 
	 * @param 	daysToMove
	 * 			The number of days the entry should be moved.
	 * @post	The day on which this entry registered will be moved forward or backward 
	 * 			according to daysToMove.
	 * 			|new.getDay() = this.getDay() + daysToMove
	 * @throws 	IllegalArgumentException
	 * 			If the move will result in overlapping.
	 * 			|(!agenda.slotsAvailable(this.getDay()+daysToMove, this.getAllSlots()))
	 * 			Or if the result will be out of range.
	 * 			|((Long.MAX_VALUE - this.getDay() < daysToMove) ||
	 * 			|(this.getDay() + daysToMove < 0) ||
	 * 			|(this.getDay() + daysToMove > getMaxDay())
	 */
	@Override
	public void moveNbDays(long daysToMove) throws IllegalArgumentException{
		if (Long.MAX_VALUE - getDay() < daysToMove){
			throw new IllegalArgumentException();
		}
		if (this.getDay() + daysToMove < 0){
			throw new IllegalArgumentException();
		}
		if (this.getDay() + daysToMove > getMaxDay()){
			throw new IllegalArgumentException();
		}
		//local variable to store all associated agendas.
		Set<Agenda> relatedAgendas = getAllAgenda();
		//if overlap, throws exception
		for (Agenda agenda : relatedAgendas){
			if (!agenda.slotsAvailable(getDay() + daysToMove, getAllSlots())){
				for (Agenda removedAgenda : relatedAgendas){
					if (!removedAgenda.hasAsEntry(this)){
						removedAgenda.addEntry(this);
					}
				}//Loop again to avoid inconsistency.
				throw new IllegalArgumentException();
			}
			//remove this entry from all agenda, to reset the bucket. 
			agenda.removeEntry(this);
		}
		//reset the day.
		setDay(getDay() + daysToMove);
		//re-register into the right bucket.
		for (Agenda agenda : relatedAgendas){
			agenda.addEntry(this);
		}
	}
}//class
