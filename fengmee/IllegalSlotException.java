package fengmee;
import be.kuleuven.cs.som.annotate.*;


/**
 * A class for signaling illegal slot values for agenda entries.
 * 
 * @version  1.0
 * @author   Aimee Backiel and Feng Wu, inspired by Eric Steegmans
 */
public class IllegalSlotException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Initialize this new illegal slot exception with given value.
	 * 
	 * @param  value
	 *         The value for this new illegal slot exception.
	 * @post   The value of this new illegal slot exception is equal
	 *         to the given value.
	 *       | new.getValue() == value
	 */
	public IllegalSlotException(int value) {
		this.value = value;
	}

	/**
	 * Return the value registered for this illegal slot exception.
	 */
	@Basic
	public int getValue() {
		return this.value;
	}

	/**
	 * Variable registering the value involved in this illegal slot exception.
	 */
	private final int value;

}

