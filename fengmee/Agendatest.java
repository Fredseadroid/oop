package fengmee;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Agendatest {
	
	private Entry Entry1;
	private Entry Entry2;
	private Entry Entry3;
	private Entry Entry4;
	private Entry notInitializedEntry;
	private Agenda agendaWith2Entries;
	private Agenda agendaWithoutEntry;
	private Agenda agendaA,agendaB;

	@Before
	public void setUp() throws Exception {
		agendaA = new Agenda("agendaA");
		agendaB = new Agenda("agendaB");
		Entry1 = new PersonalEntry(agendaWith2Entries,"EntryDay11Slots1,2,3",11,1,2,3);
		Entry2 = new Meeting ("EntryDay13Slots11,12,13",13,11,3,agendaA,agendaB);
		Entry3 = new PersonalEntry(agendaWith2Entries,"EntryDay13Slots15,17",13,15,17);
		Entry4 = new Meeting("EntryDay11Slots3,4,5",11,3,3,agendaA,agendaB);
		agendaWith2Entries = new Agenda("agendaWith2Entries",Entry1,Entry3);
		agendaWithoutEntry = new Agenda();
	}

	@Test
	public void Constructor_ExtendedCase() {
		Agenda newAgenda = new Agenda("FirstAgenda",Entry2,Entry4);
		assertEquals("FirstAgenda",newAgenda.getName());
		assertFalse(newAgenda.isTerminated());
		assertTrue(newAgenda.hasProperEntries());
		assertTrue(newAgenda.getEntriesAt(13).contains(Entry2));
		assertTrue(newAgenda.getEntriesAt(11).contains(Entry4));
		assertTrue(Entry2.hasAsAgenda(newAgenda));
		assertTrue(Entry4.hasAsAgenda(newAgenda));
	}
	
	@Test
	public void Constructor_NoEntryCase(){
		Agenda emptyAgenda = new Agenda("empty agenda");
		assertTrue(emptyAgenda.hasProperEntries());
		assertFalse(emptyAgenda.isTerminated());
		assertFalse(emptyAgenda.getAllEntries().contains(Entry1));
		assertFalse(Entry1.hasAsAgenda(emptyAgenda));
	}
	
	@Test
	public void Constructor_NoParameterCase(){
		Agenda defaultAgenda = new Agenda();
		assertEquals("default",defaultAgenda.getName());
		assertTrue(defaultAgenda.hasProperEntries());
		assertFalse(defaultAgenda.isTerminated());
		assertFalse(defaultAgenda.getAllEntries().contains(Entry1));
		assertFalse(Entry1.hasAsAgenda(defaultAgenda));
	}
	
	@Test
	public void terminate_WithEntriesCase(){
		agendaWith2Entries.terminate();
		assertTrue(agendaWith2Entries.isTerminated());
		assertFalse(agendaWith2Entries.getAllEntries().contains(Entry1));
		assertTrue(agendaWith2Entries.getEntriesAt(11).isEmpty());
		assertTrue(agendaWith2Entries.getEntriesAt(13).isEmpty());
		assertFalse(Entry1.hasAsAgenda(agendaWith2Entries));
		assertFalse(Entry3.hasAsAgenda(agendaWith2Entries));
	}
	
	@Test
	public void terminate_WithoutEntryCase(){
		agendaWithoutEntry.terminate();
		assertTrue(agendaWithoutEntry.isTerminated());
	}
	
	@Test 
	public void setName_FalseCase()  {
		agendaWith2Entries.setName("");
		assertFalse(agendaWith2Entries.getName().equals(""));
		assertEquals(agendaWith2Entries.getName(),"agendaWith2Entries");
	}
	
	@Test 
	public void setName_TrueCase()  {
		agendaWith2Entries.setName("Better Name");
		assertEquals(agendaWith2Entries.getName(),"Better Name");
	}
	
	@Test
	public void slotsAvailable_TrueCase() {
		assertTrue(agendaWith2Entries.slotsAvailable(11,5,6));
	}
	
	@Test
	public void slotsAvailable_FalseCase() {
		assertFalse(agendaWith2Entries.slotsAvailable(11,1,2));
	}
	
	@Test
	public void slotsAvailable_NegativeCase() {
		assertFalse(agendaWith2Entries.slotsAvailable(-5,5,6));
	}
	@Test
	public void slotsAvailable_GreaterThanMaximumCase() {
		assertFalse(agendaWith2Entries.slotsAvailable(5000,5,6));
	}
	
	@Test
	public void getAllEntries_SingleCase(){
		assertEquals(agendaWith2Entries.getAllEntries().size(),2);
		assertTrue(agendaWith2Entries.getAllEntries().contains(Entry1));
		assertTrue(agendaWith2Entries.getAllEntries().contains(Entry3));
		assertFalse(agendaWith2Entries.getAllEntries().contains(Entry2));
	}
	
	@Test
	public void canHaveAsEntry_TrueCase(){
		assertTrue(agendaWith2Entries.canHaveAsEntry(Entry2));
	}
	
	@Test
	public void canHaveAsEntry_FalseOverlappedCase(){
		assertFalse(agendaWith2Entries.canHaveAsEntry(Entry4));
	}
	
	@Test
	public void canHaveAsEntry_FalseNotInitializedCase(){
		assertFalse(agendaWith2Entries.canHaveAsEntry(notInitializedEntry));
	}
	
	@Test
	public void addEntry_ValidCase(){
		agendaWith2Entries.addEntry(Entry2);
		assertTrue(agendaWith2Entries.hasAsEntry(Entry2));
		assertTrue(Entry2.hasAsAgenda(agendaWith2Entries));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void addEntry_InvalidCase() throws Exception {
		agendaWith2Entries.addEntry(Entry4);
	}
	
	@Test
	public void purgeEntry_ValidCase(){
		agendaWith2Entries.purgeEntry(Entry1);
		assertFalse(agendaWith2Entries.hasAsEntry(Entry1));
		assertFalse(Entry1.hasAsAgenda(agendaWith2Entries));
		assertTrue(agendaWith2Entries.hasAsEntry(Entry3));
		assertTrue(Entry3.hasAsAgenda(agendaWith2Entries));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void purgeEntry_InvalidCase() throws Exception {
		agendaWith2Entries.purgeEntry(Entry2);
	}
}
