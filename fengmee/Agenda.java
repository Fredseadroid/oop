package fengmee;

import be.kuleuven.cs.som.annotate.*;

import java.util.*;

/**
 * A class of agendas with names and a series of entries.
 * 
 * @invar	The agenda must associate with entries properly.
 * 			|hasProperEntries()
 * @invar	The name of the agenda must be a valid name.
 * 			|isValidName(getName())
 * 
 * @version	3.6
 * @author	Aimee Backiel and Feng Wu
 */

public class Agenda {
	
	/**
	 * Initialize this new agenda with given name and a series of entries.
	 * 
	 * @param 	name
	 * 			The name of this new agenda.
	 * @param 	entryList
	 * 			The series of entries which are associated with this new agenda.
	 * @effect	...
	 * 			|setName(name)
	 * @effect	...
	 * 			|For each entry in entryList
	 * 			|	addEntry(entry)
	 */
	public Agenda (String name, Entry... entryList) throws IllegalArgumentException {
		setName(name);
		for (Entry entry : entryList){
			addEntry(entry);
		}
	}
	
	/**
	 * Initialize this new agenda with given name and no associated entries.
	 * 
	 * @param 	name
	 * 			The name of this new agenda.
	 * @effect	...
	 * 			|Agenda(name,new Entry[0])
	 */
	public Agenda (String name){
		this(name,new Entry[0]);
	}
	
	/**
	 * Initialize this new agenda with the default name and no associated entries.
	 * 
	 * @effect	...
	 * 			|Agenda("")
	 */
	public Agenda (){
		this("");
	}
	
	/**
	 * Check whether this agenda is already terminated.
	 */
	@Basic
	public boolean isTerminated(){
		return this.isTerminated;
	}//getter

	/**
	 * Terminate this agenda.
	 * 
	 * @post	This agenda is terminated.
	 * 			|new.isTerminated()
	 * @effect	The entries belonging to this agenda, will be removed from this agenda.
	 * 			|For each entry in getAllEntries()
	 * 			|	this.removeEntry(entry)
	 */
	public void terminate(){
		if (!isTerminated()){
			for (Entry entry : getAllEntries()){
				removeEntry(entry);
			}
			this.isTerminated = true;
		}
	}//setter
	
	/**
	 * Variable registering whether or not this agenda has been terminated.
	 */
	private boolean isTerminated = false;
	
	/**
	 * Return the maximum number of days that can be registered for this agenda.
	 */
	@Basic @Immutable
	public long getMaxDay(){
		return this.maxDay;
	}

	/**
	 * Check whether the provided day is registrable for this agenda.
	 * 
	 * @param 	day
	 * 			The day to be checked for this agenda.
	 * @return	...
	 * 			|result == 
	 * 			|			(0 <= day) && (day <= getMaxDay())
	 */
	public boolean canHaveAsDay(long day){
		return (0 <= day) && (day <= getMaxDay());
	}
	
	/**
	 * The variable that store the maximum number of days that can be
	 * registered for this agenda.
	 */
	private final long maxDay = 2999;
	
	/**
	 * Return the name of this agenda.
	 */
	@Basic
	public String getName(){
		return this.name;
	}//getter
	
	/**
	 * Set the name for this agenda.
	 * 
	 * @param	newName
	 * 			The new name for this agenda.
	 * @post	The new name of the agenda is the supplied name.
	 * 			|if (isValidName(newName))
	 * 			|	new.getName().equals(newName)
	 */
	public void setName(String newName){
		if (isValidName(newName)){
			this.name = newName;
		}
	}//setter

	/**
	 * Check if the provided name is valid for the agenda.
	 * 
	 * @param 	name
	 * 			The string to be checked as a valid name for an agenda.
	 * @return	...
	 * 			|result == (!name.equals(""))
	 */
	public static boolean isValidName(String name){
		return (!name.equals(""));
	}//checker
	
	/**
	 * The string variable that stores the name of the agenda.
	 * The default name is "default".
	 */
	private String name ="default";
	
	/**
	 * Check whether the provided time slot is available.
	 * 
	 * @param 	day
	 * 			The day to be checked for availability.
	 * @param	slot
	 * 			The time slot to be checked for availability.
	 * @return	...
	 * 			|if(!canHaveAsDay(day))
	 * 			|	result == false
	 * @return	...
	 * 			|result == 
	 * 			|	(for each entry in getEntriesAt(day):
	 * 			|		!entry.isOccupied(slot) )
	 */
	public boolean slotAvailable(long day, int slot){
		if (!canHaveAsDay(day)){
			return false;
		}
		for (Entry entry : getEntriesAt(day)){
			if (entry.isOccupied(slot)){
				return false;
			}
		}
		return true;
	}

	/**
	 * Check whether the provided time slots are available.
	 * 
	 * @param 	day
	 * 			The day to be checked for availability.
	 * @param	slots
	 * 			The time slots to be checked for availability.
	 * @return	...
	 * 			|if(!isValidDay(day)
	 * 			|	result == false
	 * @return	...
	 * 			|else if (getEntriesAt(day).isEmpty())
	 * 			|	result == true
	 * 			|else
	 * 			|	result == 
	 * 			|	(for each slot in slots:
	 * 			|		slotAvailable(day, slot) )
	 */	
	public boolean slotsAvailable(long day, int... slots){
		if (!canHaveAsDay(day)){
			return false;
		}
		if (!this.entryMap.containsKey(day)){
			return true;
		}
		for (int slot : slots){
			if (!slotAvailable(day, slot)){
				return false;
			}
		}
		return true;
	}

	/**
	 * Check if the agenda has proper entries.
	 * 
	 * @return	...
	 * 			|result ==
	 * 			|	(for each entry in getAllEntries():
	 * 			|		entry.hasAsAgenda(this) && hasAsEntry(entry) )
	 */
	public boolean hasProperEntries(){
		for (Entry entry : getAllEntries()){
			if ((!entry.hasAsAgenda(this))||(!hasAsEntry(entry))){
				return false;
			}
		}
		return true;
	}

	/**
	 * Check whether the agenda contains certain entry.
	 * 
	 * @param 	entry
	 * 			The entry to be checked if it is in the agenda.
	 * @return	If the entry is not effective, return false.
	 * 			| if(entry == null)
	 * 			|		result == false
	 * @return	Else if and only if the agenda contains entry, return true.
	 * 			| else result == getAllEntries().contains(entry)
	 */
	public boolean hasAsEntry(Entry entry){
		if (entry == null){
			return false;	
		}
		long day = entry.getDay();
		if (!this.entryMap.containsKey(day)){
			return false;
		}
		else if (this.entryMap.get(day).contains(entry)){
			return true;
		}
		return false;
	}

	/**
	 * Get all entries registered for the designated day.
	 * 
	 * @param 	day
	 * 			The day in which all the entries registered will be retrieved. 
	 * @return 	a Set containing all the entries belonging to the designated day.
	 * 			| for each entry in getAllEntries()
	 * 			| 	if(entry.getDay() == day)
	 * 			|		result.contains(entry) == true
	 * 			|	else
	 * 			}		result.contains(entry) == false
	 */
	public Set<Entry> getEntriesAt(long day){
		Set<Entry> entriesAtDay = new HashSet<Entry>();
		if (this.entryMap.containsKey(day)){
			entriesAtDay.addAll(this.entryMap.get(day));
		}
		return entriesAtDay;
	}

	/**
	 * return a Set containing all the entries belonging to the agenda.
	 */
	@Basic
	public Set<Entry> getAllEntries(){
		Set <Entry> allEntries = new HashSet<Entry>();
		for (long day : this.entryMap.keySet()){
			allEntries.addAll(getEntriesAt(day));
		}
		return allEntries;
	}//getter

	/**
	 * Check whether the agenda can have the provided entry.
	 * 
	 * @param 	entry
	 * 			The entry to be checked if it's allowed for this agenda.
	 * @return	...
	 * 			|if ((entry == null) || (!entry.canHaveAsAgenda(this)))
	 * 			|	result == false
	 * @return	If the entry is not initialized, return false.
	 * 			|if (entry.getDay() == -1)
	 * 			|	result == false
	 * 			|else
	 * 			|	result ==
	 * 			|	(for each existEntry in getAllEntries():
	 * 			|		!existEntry.overlapsWith(entry) )
	 */
	public boolean canHaveAsEntry(Entry entry){
		if ((entry == null) || (!entry.canHaveAsAgenda(this))){
			return false;
		}
		long day = entry.getDay();
		if (day == -1){
			return false;
		}// the default value of day is -1, indicating the entry is not initialized.
		if (!this.entryMap.containsKey(day)){
			return true;
		}
		for (Entry existEntry : this.entryMap.get(day)){
			if (existEntry.overlapsWith(entry))
				return false;
		}
		return true;
	}

	/**
	 * Add a given entry to this agenda.
	 * 
	 * @param 	entry
	 * 			the entry that will be associated to this agenda.
	 * @post	...
	 * 			|new.getAllEntries().contains(entry) == true
	 * 			|(new entry).hasAsAgenda(this) == true
	 * @throws 	IllegalArgumentException
	 * 			The agenda cannot have the provided entry as an entry.
	 * 			|!canHaveAsEntry(entry)
	 */
	public void addEntry(Entry entry) throws IllegalArgumentException{
		if (!canHaveAsEntry(entry)){
			throw new IllegalArgumentException();
		}
		long day = entry.getDay();
		if (this.entryMap.containsKey(day)){
			this.entryMap.get(day).add(entry);
		}
		else{
			this.entryMap.put(day, new HashSet<Entry>());
			this.entryMap.get(day).add(entry);
		}
		entry.putInAgenda(this);
	}
	
	/**
	 * Permanently removes a given entry from this agenda
	 * 	and terminates the entry if it is associated with no agendas.
	 * 
	 * @param 	entry
	 * 			the entry that will be removed from this agenda
	 * @effect	...
	 * 			|removeEntry(entry)
	 * @post	...
	 * 			|if (entry.getAllAgenda().isEmpty())
	 * 			|	(new entry).isTerminated == true
	 */
	public void purgeEntry(Entry entry) throws IllegalArgumentException{
		this.removeEntry(entry);
		//check if the entry still associate with other agendas, if not, terminate.
		if (entry.getAllAgenda().isEmpty()){
			entry.terminate();
		}
	}
	
	/**
	 * Remove a given entry from this agenda.
	 * 
	 * @param 	entry
	 * 			the entry that will be disconnected from this agenda.
	 * @post	...
	 * 			|new.getAllEntries().contains(entry) == false
	 * 			|(new entry).hasAsAgenda(this) == false
	 * @throws 	IllegalArgumentException
	 * 			The agenda does not have the given entry as an entry.
	 * 			|!hasAsEntry(entry)
	 */
	void removeEntry(Entry entry) throws IllegalArgumentException{
		if (!hasAsEntry(entry)){
			throw new IllegalArgumentException();
		}
		long day = entry.getDay();
		this.entryMap.get(day).remove(entry);
		entry.removeFromAgenda(this);
		//if the bucket is empty, remove the bucket
		if (this.entryMap.get(day).isEmpty()){
			this.entryMap.remove(day);
		}
	}

	/**
	 * The map that tracks the days, and each entry on each day.
	 * 
	 * @invar	The map of entries is not null.
	 * 			| entryMap != null
	 * @invar	The existing set in this map is not empty.
	 * 			|for each key in entryMap.keySet()
	 * 			|	!entryMap.get(key).isEmpty()
	 * @invar	The entries in the Map refer to this agenda.
	 * 			|for each hashSet in entryMap
	 * 			|	for each entry in hashSet
	 * 			|		entry.hasAsAgenda(this)
	 * @invar	The entries in the Map do not overlap with each other.
	 * 			|for each hashSet in entryMap
	 * 			|	for each entry and otherEntry in hashSet
	 * 			|		!entry.overlapsWith(otherEntry)
	 * 
	 * @note	Usually should be Set<Entry> => HashSet<Entry>
	 * 			Java does not allow this, so HashSet is used here.
	 */
	private final Map<Long, HashSet<Entry>> entryMap = new HashMap<Long,HashSet<Entry>>();
}
