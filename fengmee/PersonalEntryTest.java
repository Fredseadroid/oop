package fengmee;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PersonalEntryTest {
	private Agenda agenda1;
	private PersonalEntry PeDay8Slot46;

	@Before
	public void setUp() throws Exception {
		agenda1 = new Agenda("agenda1");
		PeDay8Slot46 = new PersonalEntry(agenda1,"PeDay8Slot46",8,4,6);
	}

	@Test
	public void testPersonalEntryAgendaStringLongIntArray() {
		PersonalEntry newPersonalEntry = new PersonalEntry(agenda1,"newPersonalEntry",10,3,5,7);
		assertEquals(newPersonalEntry.getDay(),10);
		assertTrue(newPersonalEntry.getDescription().equals("newPersonalEntry"));
		assertEquals(newPersonalEntry.getAllSlots().length,3);
		assertEquals(newPersonalEntry.getAllSlots()[0],3);
		assertEquals(newPersonalEntry.getAllSlots()[1],5);
		assertEquals(newPersonalEntry.getAllSlots()[2],7);
		assertTrue(newPersonalEntry.hasAsAgenda(agenda1));
	}

	@Test
	public void testPersonalEntryAgendaStringLong() {
		PersonalEntry newPersonalEntry = new PersonalEntry(agenda1,"newPersonalEntry",10);
		assertEquals(newPersonalEntry.getDay(),10);
		assertTrue(newPersonalEntry.getDescription().equals("newPersonalEntry"));
		assertEquals(newPersonalEntry.getAllSlots().length,24);
		assertEquals(newPersonalEntry.getAllSlots()[0],1);
		assertEquals(newPersonalEntry.getAllSlots()[23],24);
		assertTrue(newPersonalEntry.hasAsAgenda(agenda1));
	}

	@Test
	public void testHasProperAgendas() {
		PeDay8Slot46.hasProperAgendas();
	}

}
