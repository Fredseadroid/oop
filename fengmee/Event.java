package fengmee;

import java.util.Set;

/**
 * A class of events with long days and string descriptions.
 * Sub-class of Entry.
 * 
 * @invar	The day of each entry must be a valid day.
 * 			|canHaveAsDay(getDay())
 * @invar	The description of the entry must be a valid string.
 * 			|isValidDescription(getDescription())
 * @invar	The entry must be associated with proper agendas.
 * 			|hasProperAgendas()
 * 
 * @version	3.6
 * @author	Aimee Backiel and Feng Wu
 */

public class Event extends Entry {

	/**
	 * Initialize this new event with given description, agenda and day.
	 * 
	 * @param	agenda
	 * 			The agenda which the event will be registered in.
	 * @param 	description
	 * 		  	The description of this new agenda event.
	 * @param 	day
	 * 		  	The day of this new agenda event.
	 * @effect	The day will be registered using setDay(day)
	 * 			|setDay(day)
	 * @effect	The description will be registered using setDescription(description)
	 * 			|setDescription(description)
	 * @effect	If the provided agenda is not null, register it using agenda.addEntry(this)
	 * 			|if (agenda != null)
	 * 			|	agenda.addEntry(this)
	 * @note	The day of the event must be initialized before being added to any agenda.
	 */
	public Event(Agenda agenda, String description, long day)throws IllegalArgumentException {
		setDescription(description);
		setDay(day);
		if (agenda != null){
			agenda.addEntry(this);
		}
	}

	/**
	 * Initialize this new entry with given description and day, to occupy all time slots of that day.
	 * 
	 * @param 	description
	 * 		  	The description of this new agenda event.
	 * @param 	day
	 * 		  	The day of this new agenda event.
	 * @effect	...
	 * 			|Event(null, description, day)
	 */
	public Event(String description, long day) {
		this(null,description,day);
	}
	
	/**
	 * Check whether this entry associates agendas properly.
	 * 
	 * @return	All the agendas associated with this entry must also link back to this entry.
	 * 			| result ==
	 * 			|	(for each agenda in getAllAgenda():
	 * 			|		agenda.hasAsEntry(this) )
	 */
	@Override
	public boolean hasProperAgendas(){
		return super.hasProperAgendas();
	}
	
	/**
	 * Check whether this entry can be added to the provided agenda.
	 *  
	 * @param 	agenda
	 * 			The agenda to be checked.
	 * @return	...
	 * 			|	result == (agenda != null) && (!this.isTerminated())
	 */
	@Override
	protected boolean canHaveAsAgenda(Agenda agenda){
		return super.canHaveAsAgenda(agenda);
	}
	
	/**
	 * check whether this entry starts before some other entry.
	 * 
	 * @param 	other
	 * 			The other entry to be checked.
	 * @return	True if and only if the day registered for the current entry 
	 * 			is earlier than the other one.
	 * 			|	result == (this.getDay() < other.getDay())
	 */
	@Override
	public boolean startsBefore(Entry other){
		return super.startsBefore(other);
	}

	/**
	 * Events never overlap with other entries.
	 * 
	 * @return 	...
	 * 			| result == false
	 */
	@Override
	public final boolean overlapsWith(Entry other) {
		return false;
	}
	
	/**
	 * Move the entry's day forward or backward the given number of days.
	 * 
	 * @param 	daysToMove
	 * 			The number of days the entry should be moved.
	 * @post	The day on which this entry registered will be moved forward or backward 
	 * 			according to daysToMove.
	 * 			|new.getDay() = this.getDay() + daysToMove
	 * @throws 	IllegalArgumentException
	 * 			If the result will be out of range.
	 * 			|((Long.MAX_VALUE - this.getDay() < daysToMove) ||
	 * 			|(this.getDay() + daysToMove < 0) ||
	 * 			|(this.getDay() + daysToMove > getMaxDay())
	 */
	@Override
	public void moveNbDays(long daysToMove)  throws IllegalArgumentException{
		if (Long.MAX_VALUE - getDay() < daysToMove){
			throw new IllegalArgumentException();
		}
		if (this.getDay() + daysToMove < 0){
			throw new IllegalArgumentException();
		}
		if (this.getDay() + daysToMove > getMaxDay()){
			throw new IllegalArgumentException();
		}
		//local variable to store all associated agendas.
		Set<Agenda> relatedAgendas = getAllAgenda();

		for (Agenda agenda : relatedAgendas){
			//remove this entry from all agenda, to reset the bucket.
			agenda.removeEntry(this);
		}
		//reset the day.
		setDay(getDay() + daysToMove);
		//re-register into the right bucket.
		for (Agenda agenda : relatedAgendas){
			agenda.addEntry(this);
		}
	}
	
	/**
	 * Event occupies no time slots.
	 */
	@Override
	public boolean isOccupied(int slot){
		return false;
	}

}
