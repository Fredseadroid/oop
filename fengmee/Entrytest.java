package fengmee;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Entrytest {
	
	private Entry EventDay10Holiday;
	private Entry EventDay10Birthday;
	private Entry EventDay9;
	private Entry EventDay11;
	private Agenda agenda1, agenda2;
	
	@Before
	public void setUp(){
		agenda1 = new Agenda();
		agenda2 = new Agenda("agenda2");
		EventDay10Holiday = new Event("EventDay10Holiday",10);
		EventDay10Birthday = new Event(agenda2,"EventDay10Birthday",10);
		EventDay9 = new Event("EventDay9",9);
		EventDay11 = new Event(agenda1,"EventDay11",11);
	}
	
	@Test
	public void terminate_WithAgendaCase(){
		EventDay10Birthday.terminate();
		assertTrue(EventDay10Birthday.isTerminated());
		assertFalse(EventDay10Birthday.hasAsAgenda(agenda2));
		assertFalse(agenda2.hasAsEntry(EventDay10Birthday));
	}
	
	@Test
	public void terminate_WithoutAgendaCase(){
		EventDay10Holiday.terminate();
		assertTrue(EventDay10Holiday.isTerminated());
		assertFalse(EventDay10Holiday.hasAsAgenda(agenda1));
		assertFalse(agenda1.hasAsEntry(EventDay10Holiday));
	}
	
	@Test
	public void MaxDay_singleCase(){
		assertEquals(2999,EventDay10Holiday.getMaxDay());
	}
	
	@Test
	public void isValidDay_TrueCase(){
		assertTrue(EventDay10Holiday.canHaveAsDay(1000));
	}
	
	@Test
	public void isValidDay_FalseCase(){
		assertFalse(EventDay10Holiday.canHaveAsDay(5000));
	}
	
	@Test
	public void getDay_SingleCase(){
		assertEquals(10,EventDay10Holiday.getDay());
	}
	
	@Test
	public void getDescription_SingleCase(){
		assertEquals("EventDay10Holiday",EventDay10Holiday.getDescription());
	}
	
	@Test
	public void setDescription_SingleCase(){
		EventDay10Holiday.setDescription("New Description");
		assertEquals("New Description",EventDay10Holiday.getDescription());
	}
	
	@Test
	public void isValidDescription_TrueCase(){
		assertTrue(Entry.isValidDescription("'Bob's \"101st_Entry\""));
	}
	
	@Test
	public void isValidDescription_FalseCase(){
		assertFalse(Entry.isValidDescription("1001 Arabian Nights"));
	}
	
	@Test
	public void startsBefore_TrueCase(){
		assertTrue(EventDay10Holiday.startsBefore(EventDay11));
	}
	
	@Test
	public void startsBefore_FalseCase(){
		assertFalse(EventDay11.startsBefore(EventDay9));
	}
	
	@Test
	public void hasProperAgendas_nonTerminatedCase(){
		assertTrue(EventDay11.hasProperAgendas());
	}
	
	@Test
	public void hasProperAgendas_terminatedCase(){
		EventDay11.terminate();
		assertTrue(EventDay11.hasProperAgendas());
	}
	
	@Test
	public void hasAsAgenda_trueCase(){
		assertTrue(EventDay11.hasAsAgenda(agenda1));
	}
	
	@Test
	public void hasAsAgenda_falseCase(){
		assertFalse(EventDay9.hasAsAgenda(agenda2));
	}
	
	@Test
	public void getAllAgenda_singleCase(){
		assertTrue(EventDay11.getAllAgenda().contains(agenda1));
		assertTrue(EventDay10Birthday.getAllAgenda().contains(agenda2));
		assertFalse(EventDay9.getAllAgenda().contains(agenda2));
	}
}
