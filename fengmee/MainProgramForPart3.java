package fengmee;

/**
 * Main program
 * 
 * @version	1.0
 * @author	Aimee Backiel and Feng Wu
 */

public class MainProgramForPart3 {

	public static void main(String[] args) {
		//---------------------------
		Agenda Alice = new Agenda("Alice's agenda");
		Event momBirthday = new Event(Alice,"Mom's 65th Birthday",80);
		Agenda Bob = new Agenda("Bob's agenda");
		Meeting AlicAndBob = new Meeting("Meeting of Alice and Bob",11,12,2,Alice,Bob);
		Agenda Carol = new Agenda("Carol");
		Meeting AliceBobCarol = new Meeting("Meeting of Alice, Bob and Carol",12,10,2,Alice,Bob,Carol);
		try{
			PersonalEntry tennisTraining = new PersonalEntry(Alice,"Tennis Training",11,11,12,18);
		}
		catch (IllegalArgumentException exc) {
			System.out.println("Cannot register because of overlapping.");
		}
		
		PersonalEntry dinnerWithMom = new PersonalEntry(Alice,"Dinner with Mom",80,19,20);
		
		//--------------------------
		System.out.println("\nBob's agenda contains:");
		for(Entry entry : Bob.getAllEntries()){
			System.out.print(entry.getDescription()+" \nday: "+entry.getDay());
			if (entry instanceof SlottedEntry){
				System.out.print("  Time:");
				int[] slots = ((SlottedEntry)entry).getAllSlots();
				for (int slot : slots){
					System.out.print("  "+slot);
				}
			}
			System.out.println();
		}
		
		//--------------------------
		System.out.println("\nAlice's agenda contains:");
		for(Entry entry : Alice.getAllEntries()){
			System.out.print(entry.getDescription()+" \nday: "+entry.getDay());
			if (entry instanceof SlottedEntry){
				System.out.print("  Time:");
				int[] slots = ((SlottedEntry)entry).getAllSlots();
				for (int slot : slots){
					System.out.print("  "+slot);
				}
			}
			System.out.println();
		}
	}
}
