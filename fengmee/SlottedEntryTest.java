package fengmee;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SlottedEntryTest {
	private Agenda agenda1, agenda2;
	private SlottedEntry PeDay10Slot567;
	private SlottedEntry PeDay11Slot567;
	private SlottedEntry PeDay9Slot567;
	private SlottedEntry PeDay10Slot123;
	private SlottedEntry PeDay10Slot12345;
	private SlottedEntry MeetingDay8Slot345;

	@Before
	public void setUp() throws Exception {
		agenda1 = new Agenda("agenda1");
		agenda2 = new Agenda("agenda2");
		PeDay10Slot567 = new PersonalEntry(agenda1,"PeDay10Slot567",10,5,6,7);
		PeDay11Slot567 = new PersonalEntry(agenda1,"PeDay11Slot567",11,5,6,7);
		PeDay9Slot567 = new PersonalEntry(agenda1,"PeDay9Slot567",9,5,6,7);
		PeDay10Slot123 = new PersonalEntry(agenda1,"PeDay10Slot123",10,1,2,3);
		PeDay10Slot12345 = new PersonalEntry(agenda2,"PeDay10Slot12345",10,1,2,3,4,5);
		MeetingDay8Slot345 = new Meeting("MeetingDay8Slot345",8,3,3,agenda1,agenda2);
	}
	
	@Test
	public void testGetSlotsPerDay() {
		assertEquals(24,SlottedEntry.getSlotsPerDay());
	}

	@Test
	public void testIsValidSlot_TrueCase() {
		assertTrue(SlottedEntry.isValidSlot(23));
	}
	
	@Test
	public void testIsValidSlot_FalseCase() {
		assertFalse(SlottedEntry.isValidSlot(-1));
		assertFalse(SlottedEntry.isValidSlot(40));
	}

	@Test
	public void testIsOccupied() {
		assertTrue(PeDay10Slot567.isOccupied(5));
		assertFalse(PeDay10Slot567.isOccupied(3));
	}

	@Test
	public void testGetAllSlots() {
		assertEquals(5,PeDay10Slot567.getAllSlots()[0]);
		assertEquals(6,PeDay10Slot567.getAllSlots()[1]);
		assertEquals(7,PeDay10Slot567.getAllSlots()[2]);
		assertEquals(3,PeDay10Slot567.getAllSlots().length);
	}

	@Test
	public void testGetFirstSlot() {
		assertEquals(PeDay10Slot567.getFirstSlot(),PeDay10Slot567.getAllSlots()[0]);
		assertEquals(PeDay10Slot567.getFirstSlot(),5);
	}

	@Test
	public void testGetLastSlot() {
		assertEquals(PeDay10Slot567.getLastSlot(),PeDay10Slot567.getAllSlots()[2]);
		assertEquals(PeDay10Slot567.getLastSlot(),7);
	}

	@Test
	public void testGetNbSlots() {
		assertEquals(PeDay10Slot567.getNbSlots(),PeDay10Slot567.getAllSlots().length);
		assertEquals(PeDay10Slot567.getNbSlots(),3);
	}

	@Test
	public void testOverlapsWith_trueCase() {
		assertTrue(PeDay10Slot12345.overlapsWith(PeDay10Slot567));
	}
	
	@Test
	public void testOverlapsWith_falseCase() {
		assertFalse(PeDay10Slot123.overlapsWith(PeDay10Slot567));
		assertFalse(PeDay9Slot567.overlapsWith(PeDay10Slot567));
	}

	@Test
	public void testStartsBefore_trueCase() {
		assertTrue(PeDay10Slot123.startsBefore(PeDay10Slot567));
		assertTrue(PeDay9Slot567.startsBefore(PeDay10Slot567));
	}
	
	@Test
	public void testStartsBefore_falseCase() {
		assertFalse(PeDay10Slot123.startsBefore(PeDay9Slot567));
		assertFalse(PeDay11Slot567.startsBefore(PeDay10Slot567));
	}

	@Test
	public void testAddSlots_validSingleSlotCase() {
		PeDay10Slot123.addSlots(4);
		assertEquals(PeDay10Slot123.getLastSlot(),4);
		assertEquals(PeDay10Slot123.getAllSlots().length,4);
	}
	
	@Test
	public void testAddSlots_validMultipleSlotsCase() {
		PeDay10Slot567.addSlots(8,9,10,11);
		assertEquals(PeDay10Slot567.getLastSlot(),11);
		assertEquals(PeDay10Slot567.getAllSlots()[3],8);
		assertEquals(PeDay10Slot567.getAllSlots()[4],9);
		assertEquals(PeDay10Slot567.getAllSlots()[5],10);
		assertEquals(PeDay10Slot567.getAllSlots()[6],11);
		assertEquals(PeDay10Slot567.getAllSlots().length,7);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testAddSlots_invalidSingleSlotCase() throws Exception {
		PeDay10Slot123.addSlots(5);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testAddSlots_invalidMultipleSlotsCase() throws Exception {
		PeDay10Slot567.addSlots(2,3,4);
	}

	@Test
	public void testRemoveSlot_validCase() {
		PeDay10Slot567.removeSlot(5);
		PeDay10Slot567.removeSlot(6);
		assertEquals(PeDay10Slot567.getAllSlots().length,1);
		assertEquals(PeDay10Slot567.getFirstSlot(),7);
		assertEquals(PeDay10Slot567.getLastSlot(),7);
	}
	
	@Test(expected = IllegalModificationException.class)
	public void testRemoveSlot_invalidCase() throws Exception {
		PeDay10Slot567.removeSlot(3);
	}

	@Test
	public void testGetAllAgenda() {
		assertTrue(PeDay10Slot567.getAllAgenda().contains(agenda1));
		assertEquals(PeDay10Slot567.getAllAgenda().size(),1);
		assertTrue(MeetingDay8Slot345.getAllAgenda().contains(agenda2));
		assertTrue(MeetingDay8Slot345.getAllAgenda().contains(agenda1));
		assertEquals(MeetingDay8Slot345.getAllAgenda().size(),2);
	}

	@Test
	public void testHasProperAgendas() {
		assertTrue(MeetingDay8Slot345.hasProperAgendas());
	}

	@Test
	public void testHasAsAgenda_trueCase() {
		assertTrue(MeetingDay8Slot345.hasAsAgenda(agenda1));
		assertTrue(MeetingDay8Slot345.hasAsAgenda(agenda2));
	}
	
	@Test
	public void testHasAsAgenda_falseCase() {
		assertFalse(PeDay10Slot12345.hasAsAgenda(agenda1));
		assertFalse(PeDay10Slot567.hasAsAgenda(agenda2));
	}

	@Test
	public void testMoveNbDays_validForwardCase() {
		PeDay11Slot567.moveNbDays(3);
		assertEquals(PeDay11Slot567.getDay(),14);
		assertTrue(agenda1.getEntriesAt(14).contains(PeDay11Slot567));
	}
	
	@Test
	public void testMoveNbDays_validBackwardCase() {
		PeDay11Slot567.moveNbDays(-9);
		assertEquals(PeDay11Slot567.getDay(),2);
		assertTrue(agenda1.getEntriesAt(2).contains(PeDay11Slot567));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testMoveNbDays_invalidForwardCase() throws Exception {
		PeDay11Slot567.moveNbDays(3000);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testMoveNbDays_invalidForwardCase2() throws Exception {
		PeDay9Slot567.moveNbDays(2);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testMoveNbDays_invalidBackwardCase() throws Exception {
		PeDay11Slot567.moveNbDays(-19);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testMoveNbDays_invalidBackwardCase2() throws Exception {
		PeDay10Slot567.moveNbDays(-1);
	}

	@Test
	public void testMoveNbSlots_validForwardCase() {
		PeDay9Slot567.moveNbSlots(10);
		assertEquals(PeDay9Slot567.getAllSlots().length,3);
		assertEquals(PeDay9Slot567.getAllSlots()[0],15);
		assertEquals(PeDay9Slot567.getAllSlots()[1],16);
		assertEquals(PeDay9Slot567.getAllSlots()[2],17);
	}
	
	@Test
	public void testMoveNbSlots_validBackwardCase() {
		PeDay9Slot567.moveNbSlots(-3);
		assertEquals(PeDay9Slot567.getAllSlots().length,3);
		assertEquals(PeDay9Slot567.getAllSlots()[0],2);
		assertEquals(PeDay9Slot567.getAllSlots()[1],3);
		assertEquals(PeDay9Slot567.getAllSlots()[2],4);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testMoveNbSlots_invalidForwardCase() throws Exception {
		PeDay9Slot567.moveNbSlots(20);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testMoveNbSlots_invalidForwardCase_Overlap() throws Exception {
		PeDay10Slot123.moveNbSlots(3);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testMoveNbSlots_invalidBackwardCase() throws Exception {
		PeDay10Slot567.moveNbSlots(-7);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testMoveNbSlots_invalidBackwardCase_Overlap() throws Exception {
		PeDay10Slot567.moveNbSlots(-2);
	}
}
