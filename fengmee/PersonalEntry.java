/**
 * 
 */
package fengmee;

/**
 * A class of personal entries with long days, integer time slots, and string descriptions.
 * 
 * @invar	The day of each entry must be a valid day.
 * 			|canHaveAsDay(getDay())
 * @invar	The description of the entry must be a valid string.
 * 			|isValidDescription(getDescription())
 * @invar	The entry must be associated with proper agendas.
 * 			|hasProperAgendas()
 * 
 * @version	3.2
 * @author	Aimee Backiel and Feng Wu
 */
public class PersonalEntry extends SlottedEntry {

	/**
	 * Initialize this new entry with given description, day, and a series of time slot.
	 * 
	 * @param	agenda
	 * 			The agenda which the entry will be registered in.
	 * @param 	description
	 * 		  	The description of this new agenda entry.
	 * @param 	day
	 * 		  	The day of this new agenda entry.
	 * @param 	slots
	 * 		  	The series of time slot which this new entry occupied.
	 * @effect	...
	 * 			|super(agenda, description, day, slots)
	 */
	public PersonalEntry(Agenda agenda, String description, long day, int... slots) 
			throws IllegalSlotException,
			IllegalModificationException, IllegalArgumentException {
		super(agenda, description, day, slots);
	}

	/**
	 * Initialize this new entry with given description and day, 
	 * to occupy all time slots of that day.
	 * 
	 * @param 	agenda
	 * 			The agenda this personal entry belongs to.
	 * @param	description
	 * 		  	The description of this new personal entry.
	 * @param 	day
	 * 		  	The day of this new personal entry.
	 * @effect	...
	 * 			|super(description, day)
	 * @effect	...
	 * 			|agenda.addEntry(this)
	 */
	public PersonalEntry(Agenda agenda, String description, long day)
			throws IllegalSlotException, IllegalModificationException {
		super(description, day);
		agenda.addEntry(this);
	}
	
	/**
	 * Check whether this entry associates agendas properly.
	 * 
	 * @return	...
	 * 			|if(getAllAgenda().size() != 1)
	 * 			|	result == false
	 * @return	...
	 * 			|else result == super.hasProperAgendas()
	 */
	@Override
	public boolean hasProperAgendas(){
		if(getAllAgenda().size() != 1){
			return false;
		}
		return super.hasProperAgendas();
	}
	
	/**
	 * Check whether this entry can be added to the provided agenda.
	 *  
	 * @param 	agenda
	 * 			The agenda to be checked.
	 * @return	...
	 * 			|if(agenda == null)
	 * 			|	result == false
	 * @return	...
	 * 			|if (this.isTerminated())
	 * 			|	result == false
	 * @return	...
	 * 			|else result == (this.getAllAgenda().size()!=0)
	 */
	@Override
	protected boolean canHaveAsAgenda(Agenda agenda){
		return (agenda != null)&&(!this.isTerminated())&&(getAllAgenda().size()==0);
	}
	
	/**
	 * Check whether the given slots can be added to the slotted entry.
	 * 
	 * @param	slots
	 * 			The array of slots to be checked.
	 * @return	...
	 * 			|if (!super.canAddAsSlots(slots))
	 * 			|	result == false
	 * 			...
	 * 			|else result == true
	 */
	@Override
	public boolean canAddAsSlots(int... slots){
		return super.canAddAsSlots(slots);
	}
}
