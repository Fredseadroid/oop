package fengmee;

import java.util.*;

import be.kuleuven.cs.som.annotate.*;

/**
 * An abstract class of entries with long days and string descriptions.
 * 
 * @invar	The day of each entry must be a valid day.
 * 			|canHaveAsDay(getDay())
 * @invar	The description of the entry must be a valid string.
 * 			|isValidDescription(getDescription())
 * @invar	The entry must be associated with proper agendas.
 * 			|hasProperAgendas()
 * 
 * @version	3.6
 * @author	Aimee Backiel and Feng Wu
 */
abstract class Entry {
	
	/**
	 * Check whether this entry is already terminated.
	 */
	@Basic
	public boolean isTerminated(){
		return this.isTerminated;
	}

	/**
	 * Terminate this entry.
	 * 
	 * @post	The entry is terminated.
	 * 			|new.isTerminated() == true
	 * @effect	Remove any relations between this entry and agendas.
	 * 			| for each agenda in getAllAgenda()
	 * 			|	agenda.removeEntry(this)
	 */
	public void terminate(){
		for (Agenda agenda:this.agendaSet){
			agenda.removeEntry(this);
		}
		this.isTerminated = true;
	}
	
	/**
	 * Variable registering whether or not this entry has been terminated.
	 */
	private boolean isTerminated = false;
	
	/** 
	 * Return the day limit of the agenda system.
	 */
	@Basic @Immutable
	public long getMaxDay(){
		return this.maxDay;
	}//getter
	
	/**
	 * The variable stores the maximum possible day that the entry can be registered for.
	 * @note	Later on if needed we can construct corresponding setter for it.
	 */
	private final long maxDay = 2999;
		
	/**
	 * Return the day for which current entry has been registered.
	 */
	@Basic
	public long getDay(){
		return this.day;
	}//getter
	
	/**
	 * Check if the day is valid for the entry to be registered.
	 * 
	 * @return	The day must be between 0 and the maximum day.
	 * 			|result == ((day <= getMaxDay()) && (day >= 0))
	 */
	public boolean canHaveAsDay(long day){
		return ((day <= getMaxDay()) && (day >= 0));
	}//checker

	/**
	 * set the day of the entry to the new value.
	 * 
	 * @param 	day
	 * 			The day that the entry should be registered to.
	 * @pre		The provided day must be valid.
	 * 			|isValidDay(day)
	 * @post	This entry will be registered to the given day.
	 * 			|new.day = day
	 */
	protected void setDay(long day){
		assert canHaveAsDay(day);
		this.day = day;
	}//setter
	
	/**
	 * this variable records on which day the entry should be registered. 
	 * Default of -1 implies the entry is not initialized.
	 */
	private long day = -1;
	
	/**
	 * Return the description of the current entry.
	 */
	@Basic 
	public String getDescription(){
		return this.description;
	}//getter
		
	/**
	 * Set the description of the entry to the given description.
	 * 
	 * @param 	newDescription
	 * 			The string that is to be registered for the description of current entry.
	 * @pre		the newDescription should be legal
	 * 			|isValidDescription(newDescription)
	 * @post	The new description of the entry will be newDescription.
	 * 			|new.getDescription().equals(newDescription)
	 */
	public void setDescription(String newDescription){
		assert isValidDescription(newDescription);
		this.description = newDescription;
	}//setter
	
	/**
	 * Check if the given description meets the syntactical requirements.
	 * 
	 * @param 	description
	 * 			The string to be checked if is valid for description of the entry.
	 * @return	True if the first letter should be from a to z, A to Z, and quote,
	 * 			and following letters should be from a to z, A to Z, numbers, quote, double quote, underline,
	 * 			and whitespace.
	 * 			|if(description.matches("[a-zA-Z']['\"\\w\\s]*"))
	 * 			|	result == true;
	 */
	public static boolean isValidDescription(String description){
		return description.matches("[a-zA-Z']['\"\\w\\s]*");
	}//checker
	
	/**
	 * The string variable that stores the description of the entry.
	 */
	private String description;

	/**
	 * Return all the agendas this entry has been associated with.
	 */
	@Basic
	public Set<Agenda> getAllAgenda(){
		Set<Agenda> allAgenda = new HashSet<Agenda>();
		allAgenda.addAll(this.agendaSet);
		return allAgenda;
	}//getter

	/**
	 * Inspector to tell whether this entry associated to the given agenda.
	 * 
	 * @param 	agenda
	 * @return	True if this entry has associated with given agenda. 
	 * 			|result == getAllAgenda().contains(agenda)
	 */
	public boolean hasAsAgenda(Agenda agenda){
		return this.agendaSet.contains(agenda);
	}

	/**
	 * Check whether this entry associates agendas properly.
	 * 
	 * @return	All the agendas associated with this entry must also link back to this entry.
	 * 			| for some agenda in getAllAgenda():
	 * 			|	if (!agenda.hasAsEntry(this))
	 * 			|		result == false
	 */
	public boolean hasProperAgendas(){
		for (Agenda agenda : this.agendaSet){
			if (!agenda.hasAsEntry(this)){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Check whether this entry can be added to the provided agenda.
	 *  
	 * @param 	agenda
	 * 			The agenda to be checked.
	 * @return	...
	 * 			|If(agenda == null)
	 * 			|	result == false
	 * @return	...
	 * 			|If (this.isTerminated())
	 * 			|	result == false
	 */
	protected boolean canHaveAsAgenda(Agenda agenda){
		return (agenda != null)&&(!this.isTerminated());
	}

	/**
	 * Auxiliary method to set up relation with given agenda.
	 * 
	 * @param 	agenda
	 * 			the agenda that this entry should associate to.
	 * @pre		The given agenda should already associated to this entry.
	 * 			|(agenda != null) && (agenda.hasAsEntry(this))
	 * @pre		This entry has not associated with the given agenda yet.
	 * 			|!this.hasAsAgenda(agenda)
	 * @post	This entry has associated with the given agenda.
	 * 			|this.hasAsAgenda(agenda)
	 * @note	This method will only be invoked in the method of setting up association managed by agenda.
	 */
	void putInAgenda(Agenda agenda){
		assert (agenda != null) && (agenda.hasAsEntry(this));
		assert !this.hasAsAgenda(agenda);
		this.agendaSet.add(agenda);
	}
	
	/**
	 * Auxiliary method to tear down relation with give agenda.
	 * 
	 * @param 	agenda
	 * 			the agenda that this entry should disconnect to.
	 * @pre		The given agenda should already disconnected to this entry.
	 * 			|(agenda != null) && (!agenda.hasAsEntry(this))
	 * @pre		This entry has connected to the given agenda.
	 * 			|this.hasAsAgenda(agenda)
	 * @post	This entry disconnected to the given agenda.
	 * 			|!this.hasAsAgenda(agenda)
	 * @note	This method will only be invoked in the method of tearing down association managed by agenda.
	 */
	void removeFromAgenda(Agenda agenda){
		assert (agenda != null) && (!agenda.hasAsEntry(this));
		assert this.hasAsAgenda(agenda);
		this.agendaSet.remove(agenda);
	}
	
	/**
	 * A HashSet to store all the agendas this entry associates with.
	 */
	private final Set<Agenda> agendaSet = new HashSet<Agenda>();

	/**
	 * Move the day number registered for this entry for designated days.
	 */
	public abstract void moveNbDays(long daysToMove);

	/**
	 * check whether this entry overlaps with provided entry;
	 */
	public abstract boolean overlapsWith(Entry other);

	/**
	 * Check whether the slot is occupied by an entry.
	 */
	public abstract boolean isOccupied(int slot);

	/**
	 * check whether this entry starts before some other entry.
	 * 
	 * @param 	other
	 * 			The other entry to be checked.
	 * @return	False if the other entry is null.
	 * 			|if (other == null)
	 * 			|	result == false
	 * @return	True if the day registered for the current entry is earlier than the other one
	 * 			|if (this.getDay() < other.getDay())
	 * 			|	result == true
	 * @return	...
	 * 			|if (this.getDay() > other.getDay())
	 * 			|	result == false
	 */
	public boolean startsBefore(Entry other){
		if (other == null){
			return false;
		}
		if (this.getDay() < other.getDay()){
			return true;
		}
		else return false;
	}
}//class
