package fengmee;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MeetingTest {
	private Agenda agenda1, agenda2;
	private Meeting meetingDay8slot234, meetingDay8slot678;

	@Before
	public void setUp() throws Exception {
		agenda1 = new Agenda("agenda1");
		agenda2 = new Agenda("agenda2");
		meetingDay8slot678 = new Meeting ("meetingDay8slot678",8,6,3,agenda1,agenda2);
		meetingDay8slot234 = new Meeting ("meetingDay8slot234",8,2,3,agenda1,agenda2);
	}

	@Test
	public void testMeeting_validCase() {
		Meeting newMeeting = new Meeting("newMeeting",9,4,3,agenda1,agenda2);
		assertTrue(newMeeting.getDescription().equals("newMeeting"));
		assertEquals(newMeeting.getDay(),9);
		assertEquals(newMeeting.getAllSlots().length,3);
		assertEquals(newMeeting.getAllSlots()[0],4);
		assertEquals(newMeeting.getAllSlots()[1],5);
		assertEquals(newMeeting.getAllSlots()[2],6);
		assertEquals(newMeeting.getAllAgenda().size(),2);
		assertTrue(newMeeting.hasAsAgenda(agenda1));
		assertTrue(newMeeting.hasAsAgenda(agenda2));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testMeeting_invalidDurationCase() throws Exception{
		Meeting newMeeting = new Meeting("newMeeting",9,4,33,agenda1,agenda2);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testMeeting_invalidAgendaArrayCase() throws Exception{
		Meeting newMeeting = new Meeting("newMeeting",9,4,3,agenda1);
	}

	@Test
	public void testHasProperAgendas() {
		assertTrue(meetingDay8slot678.hasProperAgendas());
	}

	@Test
	public void testAddSlots_validCase() {
		meetingDay8slot678.addSlots(11,10,9);
		assertEquals(meetingDay8slot678.getAllSlots().length,6);
		assertEquals(meetingDay8slot678.getAllSlots()[0],6);
		assertEquals(meetingDay8slot678.getAllSlots()[3],9);
		assertEquals(meetingDay8slot678.getAllSlots()[4],10);
		assertEquals(meetingDay8slot678.getAllSlots()[5],11);
		assertEquals(meetingDay8slot678.getLastSlot(),11);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testAddSlots_invalidInconsecutiveCase() throws Exception{
		meetingDay8slot678.addSlots(10,11);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testAddSlots_invalidOverlappedCase() throws Exception{
		meetingDay8slot678.addSlots(4,5);
	}

	@Test
	public void testRemoveSlot_validCase() {
		meetingDay8slot678.removeSlot(6);
		assertEquals(meetingDay8slot678.getAllSlots().length,2);
		assertEquals(meetingDay8slot678.getAllSlots()[0],7);
		assertEquals(meetingDay8slot678.getAllSlots()[1],8);
		assertEquals(meetingDay8slot678.getFirstSlot(),7);
	}

	@Test (expected = IllegalArgumentException.class)
	public void testRemoveSlot_invalidCase() throws Exception{
		meetingDay8slot678.removeSlot(7);
	}
}
