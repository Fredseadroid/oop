/**
 * 
 */
package fengmee;

import java.util.*;

/**
 * A class of meetings with long days, integer time slots, and string descriptions.
 * 
 * @invar	The day of each meeting must be a valid day.
 * 			|canHaveAsDay(getDay())
 * @invar	The description of the meeting must be a valid string.
 * 			|isValidDescription(getDescription())
 * @invar	The meeting must be associated with proper agendas.
 * 			|hasProperAgendas()
 * @invar	The time slots this meeting has must be valid.
 * 			|areValidSlots(getAllSlots())
 * 
 * @version	3.6
 * @author	Aimee Backiel and Feng Wu
 */
public class Meeting extends SlottedEntry {

	/**
	 * 
	 * Initialize this new meeting with given description, day, starting slot, duration
	 * 	and associate it with one or more Agendas.
	 * 
	 * @param 	description
	 * 		  	The description of this new meeting.
	 * @param 	day
	 * 		  	The day of this new meeting.
	 * @param 	startingSlot
	 * 		  	The first time slot which this new meeting occupies.
	 * @param	duration
	 * 			The number of consecutive time slots which this new meeting will occupy.
	 * @param	agendas
	 * 			The list of agendas which the entry will be registered in.
	 * @effect	...
	 * 			|super(description, day, startingSlot)
	 * @effect	...
	 * 			|for (int i=1; i < duration; i++)
	 * 			|	this.addSlots(startingSlot+i)
	 * @effect	...
	 * 			|for each agenda in agendas :
	 * 			|	agenda.addEntry(this)
	 * @throws 	IllegalArgumentException
	 * 			The duration must be valid.
	 * 			| !isValidDuration(startingSlot, duration)
	 * 			A meeting must be associated with at least two agendas.
	 * 			| !(agendas.length < 2)
	 */
	public Meeting(String description, long day, int startingSlot, int duration, Agenda... agendas)
			throws IllegalSlotException, IllegalModificationException,
			IllegalArgumentException {
		super(description, day, startingSlot);
		if (!isValidDuration(startingSlot, duration)){
			throw new IllegalArgumentException("duration is invalid");
		}
		if (agendas.length < 2){
			throw new IllegalArgumentException("At least two agendas must be provided");
		}
		for (int i=1 ; i < duration ; i++){
			this.addSlots(startingSlot+i);
		}
		for (Agenda agenda : agendas){
			agenda.addEntry(this);
		}
	}

	/**
	 * Check whether this entry associates agendas properly.
	 * 
	 * @return	...
	 * 			|if (getAllAgenda().size < 2)
	 * 			|	result == false
	 * @return	...
	 * 			|else result == super.hasProperAgendas()
	 */
	@Override
	public boolean hasProperAgendas(){
		if (getAllAgenda().size()<2){
			return false;
		}
		return super.hasProperAgendas();
	}

	/**
	 * Check whether this entry can be added to the provided agenda.
	 *  
	 * @param 	agenda
	 * 			The agenda to be checked.
	 * @return	...
	 * 			|	result == (agenda != null) && (!this.isTerminated())
	 */
	@Override
	protected boolean canHaveAsAgenda(Agenda agenda){
		return super.canHaveAsAgenda(agenda);
	}

	/**
	 * Check if the provided time period can be registered in one day.
	 * 
	 * @param 	startingSlot
	 * 			The first slot of a consecutive series of slots.
	 * @param 	duration
	 * 			The number of slots in the consecutive series.
	 * @return  ...
	 * 			| result == 
	 * 			|		(isValidSlot(startingSlot)) && (duration > 0) 
	 * 			|		&& (getSlotsPerDay() - startingSlot >= duration)
	 */
	public static boolean isValidDuration(int startingSlot, int duration) {
		if (!isValidSlot(startingSlot))
			return false;
		if (duration <= 0)
			return false;
		else return (getSlotsPerDay() - startingSlot >= duration);
	}
	
	/**
	 * Check if a series of slots are consecutive.
	 * 
	 * @param 	slots
	 * 			An array of slots to be checked.
	 * @return	True if there is no unoccupied slot between two occupied slots.
	 * 			|result == 
	 * 			|	for (int i = 1; i < Arrays.sort(slots).length; i++)
	 * 			|		(Arrays.sort(slots)[i] - 1) == Arrays.sort(slots)[i-1]
	 */
	public static boolean areValidSlots(int... slots){
		Arrays.sort(slots);
		for (int i = 1; i < slots.length; i++){
			if ((slots[i] - 1) != slots[i-1]){
				return false;
			}
		}
		return true;
	}
		
	/**
	 * Check whether the given slots can be added to the meeting.
	 * 
	 * @param 	slots
	 * 			The array of slots to be checked
	 * @return	...
	 * 			|if (!super.canAddAsSlots(slots))
	 * 			|	result == false
	 * 			...
	 * 			|result == (areValidSlots(slots) && areValidSlots(getAllSlots())
	 * 			|&& ((Max(slots) + 1 == getFirstSlot()) || (Min(slots) - 1 == getLastSlot())))
	 */
	@Override
	public boolean canAddAsSlots (int... slots){
		if (!super.canAddAsSlots(slots)){
			return false;
		}
		int[] potentialSlots = new int[getAllSlots().length + slots.length];
		int index = 0;
		for (int i : getAllSlots()){
			potentialSlots[index] = i;
			index++;
		}
		for (int i : slots){
			potentialSlots[index] = i;
			index++;
		}
		if (!areValidSlots(potentialSlots)){
			return false;
		}
		return true;
	}
	
	/**
	 * Unregisters a slot from the meeting.
	 * 
	 * @param	slot
	 * 			The slot to be unregistered
	 * @effect	...
	 * 			| super.removeSlot(slot)
	 * @throws	IllegalArgumentException
	 * 			Only the first slot or last slot can be removed, to keep slots consecutive.
	 * 			| (slot != this.getFirstSlot()) && (slot != this.getLastSlot())
	 */
	@Override
	public void removeSlot(int slot) throws IllegalArgumentException {
		if ((slot != this.getFirstSlot()) && (slot != this.getLastSlot())){
			throw new IllegalArgumentException("not consecutive");
		}
		else super.removeSlot(slot);
	}
}//class
